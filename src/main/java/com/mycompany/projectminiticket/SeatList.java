package com.mycompany.projectminiticket;

import java.io.Serializable;
import java.util.ArrayList;

public class SeatList implements Serializable{

    private static final ArrayList<String> availableSeats = new ArrayList<>();
    private static final int rowCount = 5;      // Number of rows
    private static final int columnCount = 8;   // Number of columns
    private static String[] arrayStringAvailableSeats;
    
    public SeatList() {
        initializeSeats();
        arrayStringAvailableSeats = availableSeats.toArray(new String[0]);
    }

    private void initializeSeats() {
        for (int row = 1; row <= rowCount; row++) {
            for (int col = 1; col <= columnCount; col++) {
                availableSeats.add(String.format("%c%d", 'A' + row - 1, col));
            }
        }
    }

    public static String[] getArrayStringSeat() {
        return arrayStringAvailableSeats;
    }

    public static ArrayList<String> getAvailableSeats() {
        return availableSeats;
    }

    public static int getRowCount() {
        return rowCount;
    }

    public static int getColumnCount() {
        return columnCount;
    }

    public static String getSeatLabel(int row, int col) {
        return String.format("%c%d", 'A' + row, col);
    }

    public static boolean checkSeat(String seat) {
        return availableSeats.contains(seat);
    }

    public static void reserveSeat(String seat) {
        availableSeats.remove(seat);
    }

    public static void releaseSeat(String seat) {
        if (!availableSeats.contains(seat)) {
            availableSeats.add(seat);
        }
    }
}


