
package com.mycompany.projectminiticket;

import java.io.Serializable;

public class Movie implements Serializable{
    private String name;
    private String time;

    public Movie(String name, String time) {
        this.name = name;
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    @Override
    public String toString() {
        return name + " - " + time;
    }
}


