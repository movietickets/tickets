
package com.mycompany.projectminiticket;

import java.io.Serializable;
import java.util.ArrayList;


public class User implements Serializable{

    String tel;
    String nameMovie; 
    ArrayList<String> selectedSeat;

    public User(String tel,String nameMovie, ArrayList<String> seat) {
        this.tel = tel;
        this.nameMovie = tel;
        selectedSeat = new ArrayList<>();
    }

    public void setSeat(String seat) {
        selectedSeat.add(seat);
    }

    public void setTel(String Tel) {
        this.tel = Tel;
    }

    public void setNameMovie(String nameMovie) {
        this.nameMovie = nameMovie;
    }       

    @Override
    public String toString() {
        return "User{" + "tel=" + tel + ", nameMovie=" + nameMovie + ", selectedSeat=" + selectedSeat + '}';
    }
    
}
