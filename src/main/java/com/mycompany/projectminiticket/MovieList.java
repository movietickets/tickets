/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectminiticket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MovieList implements Serializable {

    private static List<Movie> movieList;

    public MovieList() {
        movieList = new ArrayList<>();
        addMovie("Avengers: Endgame", "12:00 PM");
        addMovie("Wonder Woman 1984", "3:30 PM");
        addMovie("Toy Story 4", "6:15 PM");
        addMovie("Spider-Man: No Way Home", "12:30 PM");
        addMovie("Black Widow", "2:45 PM");
        addMovie("The Lion King", "4:00 PM");
        addMovie("Aquaman", "7:00 PM");
        addMovie("Frozen II", "5:45 PM");
        addMovie("Doctor Strange in the Multiverse of Madness", "8:15 PM");
        addMovie("Moana", "1:45 PM");
        addMovie("นางนวล", "10.00 PM");

        Collections.sort(movieList, (Movie movie1, Movie movie2) -> movie1.getTime().compareTo(movie2.getTime()));
    }


       public final void addMovie(String name, String time) {
        // Check for duplicate time before adding
        for (Movie movie : movieList) {
            if (movie.getTime().equals(time)) {
                throw new IllegalArgumentException("A movie with the same time already exists.");
            }
        }

        Movie newMovie = new Movie(name, time);
        movieList.add(newMovie);
    }

    public List<Movie> getMovies() {
        return movieList;
    }

    public String[] getFormattedMovieNamesAndTimes() {
        ArrayList<String> formattedMovies = new ArrayList<>();
        for (Movie movie : movieList) {
            formattedMovies.add(movie.toString());
        }
        return formattedMovies.toArray(new String[0]);
    }

}
